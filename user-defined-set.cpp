
// user implemented set

#include <set>
#include <cassert>
#include <iostream>

class Number

{	public :

	class Core;  class One;  class Two;

	Number::Core * core;

	inline Number ( size_t n );
	inline Number ( size_t n, size_t m );

	inline void print ( ) const;

};  // end of  class Number


class Number::Core

// abstract class

{	public :

	size_t n;

	inline Core ( size_t nn ) : n ( nn )  {  }
	
	virtual ~Core ( )  { }

	virtual bool less_than ( const Number::Core & ) const = 0;
	virtual bool greater_than_one ( const Number::One & ) const = 0;
	virtual bool greater_than_two ( const Number::Two & ) const = 0;

	virtual void print ( ) const = 0;

};  // end of  class Number::Core


class Number::One : public Number::Core

{	public :

	// size_t n  inherited from Number::Core
	
	inline One ( size_t nn ) : Number::Core ( nn )  {  }
	
	virtual ~One ( )  { }

	bool less_than ( const Number::Core & second ) const
	{	return second .greater_than_one ( *this );  }

	bool greater_than_one ( const Number::One & second ) const;

	bool greater_than_two ( const Number::Two & second ) const;

	void print ( ) const
	{ std::cout << this->n << " ";  }

};  // end of  class Number::One


class Number::Two : public Number::Core

{	public :

	// size_t n  inherited from Number::Core
	size_t m;
	
	inline Two ( size_t nn, size_t mm ) : Number::Core ( nn ), m ( mm )  {  }
	
	virtual ~Two ( )  { }

	bool less_than ( const Number::Core & second ) const
	{	return second .greater_than_two ( *this );  }

	bool greater_than_one ( const Number::One & second ) const
	{	if ( this->n < second .n ) return false;
		else return true;                        }

	bool greater_than_two ( const Number::Two & second ) const
	{	if ( this->n > second .n ) return true;
		else return this->m > second .m;        }

	void print ( ) const
	{ std::cout << this->n << "." << this->m <<" ";  }

};  // end of  class Number::Two


bool Number::One::greater_than_one ( const Number::One & second ) const
{	return this->n > second .n;  }

bool Number::One::greater_than_two ( const Number::Two & second ) const
{	if ( this->n > second .n ) return true;
	else return false;                      }


inline Number::Number ( size_t n )
: core { new Number::One (n) }  { }

inline Number::Number ( size_t n, size_t m )
: core { new Number::Two ( n, m ) }  { }

inline void Number::print ( ) const
{	this->core->print();  }




inline bool operator< ( const Number & first, const Number & second )
{	return first .core->less_than ( * second .core );  }


int main ( )

{	std::set < Number > s;
	s .emplace ( 1, 3 );
	s .emplace ( 1 );
	s .emplace ( 2 );
	s .emplace ( 1, 1 );

	for ( std::set < Number > ::iterator it = s .begin(); it != s .end(); it++ )
		it->print();

	std::cout << std::endl;

}  // end of main


		
