
# given two hours in the format hh:mm, produce the arithmetic mean(s)
# to be used for the solar hour (first one) and sidereal hour (second one)



import sys

def usage () :
   print ( "usage : python hh:mm(solar) hh:mm(sidereal)" )
   sys .exit (1)


def  enquadra ( h_min ) :
   while  h_min < 0 :      h_min += 24*60
   while  h_min > 24*60 :  h_min -= 24*60
   return  h_min


def  format_hour ( h_min ) :
   h = int ( h_min / 60 )
   sh = str (h)
   if  len (sh) < 2 :  sh = '0' + sh
   m = h_min - h*60
   # print ( h_min, h, m )
   assert  m >= 0
   sm = str (m)
   if  len (sm) < 2 :  sm = '0' + sm
   return  sh + ':' + sm


def  daytime ( h_min ) :
   return  ( h_min > 8*60 ) and ( h_min < 19*60 )


args = sys .argv
if  len ( args ) != 3 :  usage()

solar = args [1]
solar = solar .split (':')
assert  len ( solar ) == 2
sidereal = args [2]
sidereal = sidereal .split (':')
assert  len ( sidereal ) == 2

solar_min    = int(solar[0])  *60  + int(solar[1])
sidereal_min = int(sidereal[0])*60 + int(sidereal[1])
dif_min = int ( ( sidereal_min - solar_min ) / 2 )
# print ( "dif/2", format_hour ( dif_min ) )
set_of_hmin = set()

for avrg_hour in range ( 20 ) :
   solar_hour = enquadra ( avrg_hour*60 - dif_min )
   if  daytime ( solar_hour ) :  set_of_hmin .add ( solar_hour )

for  h in sorted ( set_of_hmin ) :   
   print ( format_hour (h) )
